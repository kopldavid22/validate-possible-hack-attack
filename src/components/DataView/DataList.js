import React from "react";

import Data from "./Data";
import classes from "./DataList.module.css";

const DataList = (props) => {
  return (
    <ul className={classes["data"]}>
      {props.data.map((data) => (
        <Data
          key={data.id}
          title={data.title}
          releaseDate={data.release}
          openingText={data.openingText}
          isHacked={data.isHacked}
        />
      ))}
    </ul>
  );
};

export default DataList;
