import React, { useState, useEffect, useCallback } from "react";
import DataList from "./components/DataView/DataList";
import "./App.css";

function App() {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [isHacked, setIsHacked] = useState([]);

  const fetchDataHandler = useCallback(async () => {
    setIsLoading(true);
    setError(null);
    try {
      const response = await fetch("https://api.cyrkl.com/cdi/products");
      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const datas = await response.json();
      const loadedData = [];
      for (const key in datas) {
        //Validation for secure hack the app
        // console.log(
        //   datas[key].perex0.trim().includes("@") ||
        //     datas[key].perex0.trim().includes("+")
        // );
        if (
          datas[key].perex0.trim().includes("@") ||
          datas[key].perex0.trim().includes("+")
        ) {
          setIsHacked((prevArray) => [...prevArray, true]);
        } else {
          setIsHacked((prevArray) => [...prevArray, false]);
        }

        loadedData.push({
          id: datas[key].id,
          title: datas[key].name0,
          openingText: datas[key].perex0,
          releaseDate: datas[key].timestamp,
          isHacked: isHacked[key],
        });
      }

      setData(loadedData);
    } catch (error) {
      setError(error.message);
    }
    setIsLoading(false);
  }, [isHacked]);
  useEffect(() => {
    fetchDataHandler();
  }, [fetchDataHandler]);

  let content = <p>Found no Data.</p>;

  if (data.length > 0) {
    content = <DataList data={data} />;
  }

  if (error) {
    content = <p>{error}</p>;
  }

  if (isLoading) {
    content = <p>Loading...</p>;
  }
  return (
    <React.Fragment>
      <section>
        <button onClick={fetchDataHandler}>Fetch Data</button>
      </section>
      <section>{content}</section>
    </React.Fragment>
  );
}

export default App;
