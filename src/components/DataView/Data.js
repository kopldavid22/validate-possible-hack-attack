import React from "react";

import classes from "./Data.module.css";

const Data = (props) => {
  return (
    <li className={props.isHacked ? classes.redData : classes.data}>
      <h2>{props.title}</h2>
      <h3>{props.releaseDate}</h3>
      <p>{props.openingText}</p>
    </li>
  );
};

export default Data;
